import sys
import re
import nltk

def readInput(inputFile,depParsesFilename):
	global originalSentences
	f = open(inputFile,'r')
	line = f.readline()
	while line != None and line != '':
		line = line.strip()
		if line == '':
			line = f.readline()
			continue
		originalSentences.append(line)
		line = f.readline()
	f.close()
	global sentences
	f = open(depParsesFilename,'r')
	line = f.readline()
	sent = []
	while line != None and line != '':
		line = line.strip()
		if line == '':
			sentences.append(sent)
			sent = []
		else:
			sent.append(line)
		line = f.readline()
	f.close()

def getPOSTags(sentence):
	sentence = nltk.word_tokenize(sentence)
	sentence = nltk.pos_tag(sentence)
	print sentence
	return sentence

def createStructuredDictionary(posTaggedSent,sentence):
	wordMap = {}
	structuredSentence = []
	for branch in sentence:
		reObj = re.search(r'(.*)\((.*),(.*)\)',branch)
		if reObj:
			tokens = reObj.group(2).rsplit('-',1)
			#num = int(tokens[1])
			num = tokens[1]
			if num not in wordMap:
				wordMap[num] = {}
				wordMap[num]['word'] = tokens[0].strip()
				for pos in posTaggedSent:
					if pos[0] == tokens[0].strip():
						wordMap[num]['pos_tag'] = pos[1]
						break
			subj = num
			tokens = reObj.group(3).rsplit('-',1)
			#num = int(tokens[1])
			num = tokens[1]
			if num not in wordMap:
				wordMap[num] = {}
				wordMap[num]['word'] = tokens[0].strip()
				for pos in posTaggedSent:
					if pos[0] == tokens[0].strip():
						wordMap[num]['pos_tag'] = pos[1]
						break
			obj = num
			pred = reObj.group(1)
			structuredSentence.append({'subject':subj,'predicate':pred,'object':obj})
	return structuredSentence,wordMap

def join(structuredSentence,wordMap,predicate):
	words = {}
	removedTriples = []
	for triple in structuredSentence:
		if triple['predicate'] == predicate:
			subj = triple['subject']
			obj = triple['object']
			if subj not in words:
				words[subj] = []
			words[subj].append(obj)
			removedTriples.append(triple)
	for x in removedTriples:
		structuredSentence.remove(x)
	removedTriples = []
	for mapping in words:
		words[mapping].sort()
		phrase = ""
		pos_phrase = ""
		for word in words[mapping]:
			phrase += wordMap[word]['word'] + " "
			pos_phrase += wordMap[word]['pos_tag'] + " "
			removedTriples.append(word)
		phrase += wordMap[mapping]['word']
		pos_phrase += wordMap[mapping]['pos_tag']
		wordMap[mapping]['word'] = phrase.strip()
		wordMap[mapping]['pos_tag'] = pos_phrase.strip()
	for x in removedTriples:
		del wordMap[x]
	return structuredSentence,wordMap

def getFullNames(structuredSentence,wordMap):
	return join(structuredSentence,wordMap,'nn')

# Adjectives, Adverbs, Determiners
def ignoreTokens(structuredSentence,wordMap):
	words = {}
	removedTriples = []
	tokensToIgnore = ['amod','advmod','det']
	for triple in structuredSentence:
		if triple['predicate'] in tokensToIgnore:
			removedTriples.append(triple)
	removeFromMap = []
	for x in removedTriples:
		for mapping in wordMap:
			if x['object'] == mapping:
				removeFromMap.append(mapping)
		structuredSentence.remove(x)
	for x in removeFromMap:
		del wordMap[x]
	return structuredSentence,wordMap

def joinAuxpass(structuredSentence,wordMap):
	return join(structuredSentence,wordMap,'auxpass')

def checkIfVerb(pos_tag):
	if pos_tag.startswith('VB'):
		return True
	return False

# Does not account for the case where conj_and joins phrases, i.e. obj is a verb
def joinConjunctions(structuredSentence,wordMap):
	words = {}
	removedTriples = []
	for triple in structuredSentence:
		if triple['predicate'] == 'conj_and':
			subj = triple['subject']
			obj = triple['object']
			if obj in wordMap and 'pos_tag' in wordMap[obj] and checkIfVerb(wordMap[obj]['pos_tag'])==True:
				triple['subject'] = 0
				triple['predicate'] = 'root'
			else:
				if subj not in words:
					words[subj] = []
				words[subj].append(obj)
				removedTriples.append(triple)
	for x in removedTriples:
		structuredSentence.remove(x)
	removedTriples = []
	for mapping in words:
		words[mapping].sort()
		phrase = wordMap[mapping]['word']
		pos_phrase = wordMap[mapping]['pos_tag']
		for word in words[mapping]:
			phrase += " and " + wordMap[word]['word']
			pos_phrase +=  " CC " + wordMap[word]['pos_tag']
			removedTriples.append(word)
		wordMap[mapping]['word'] = phrase.strip()
		wordMap[mapping]['pos_tag'] = pos_phrase.strip()
	for x in removedTriples:
		del wordMap[x]
	return structuredSentence,wordMap

def getDependencies(structuredSentence,wordMap,wordIndex):
	for triple in structuredSentence:
		if triple['subject'] == wordIndex and triple['object'] in wordMap:
			print wordIndex
			print wordMap[triple['object']]
			getDependencies(structuredSentence,wordMap,triple['object'])

def convertCopToObj(structuredSentence,wordMap):
	for triple in structuredSentence:
		if triple['predicate'] == 'cop':
			subj = triple['subject']
			root_triple = None
			for triple2 in structuredSentence:
				if triple2['object'] == subj and triple2['predicate'] == 'root':
					root_triple = triple2
					break
			if root_triple != None:
				triple['predicate'] = 'root'
				triple['subject'] = 0
				root_triple['subject'] = triple['object']
				root_triple['predicate'] = 'dobj'
				for triple2 in structuredSentence:
					if triple2['subject'] == root_triple['object']:
						triple2['subject'] = triple['object']
	return structuredSentence,wordMap

def preprocess(structuredSentence,wordMap):
	try:
		structuredSentence,wordMap = createStructuredDictionary(posTaggedSent,sent)
		structuredSentence,wordMap = getFullNames(structuredSentence,wordMap)
		structuredSentence,wordMap = ignoreTokens(structuredSentence,wordMap)
		structuredSentence,wordMap = joinConjunctions(structuredSentence,wordMap)
		structuredSentence,wordMap = joinAuxpass(structuredSentence,wordMap)
		structuredSentence,wordMap = convertCopToObj(structuredSentence,wordMap)
	except KeyError:
		print "Error in sentence:"
		print structuredSentence
		print wordMap
	return structuredSentence,wordMap

def getTriples(structuredSentence,wordMap):
	triples = []
	# subj-root-obj
	nsubj = None
	root = None
	dobj = None
	marked = []
	while True:
		for triple in structuredSentence:
			if triple not in marked and triple['predicate'] == 'root':
				root = triple
				marked.append(triple)
				break
		for triple in structuredSentence:
			if triple not in marked and triple['predicate'].startswith('nsubj') and root != None and triple['subject'] == root['object']:
				nsubj = triple
				marked.append(triple)
				break
		for triple in structuredSentence:
			if triple not in marked and (triple['predicate'] == 'dobj' or triple['predicate'] == 'agent') and root != None and triple['subject'] == root['object']:
				dobj = triple
				marked.append(triple)
				break
		if nsubj != None and root != None and dobj != None:
			nsubj = wordMap[nsubj['object']]['word']
			root = wordMap[root['object']]['word']
			if dobj['predicate'] == 'agent':
				root += '_by'
			dobj = wordMap[dobj['object']]['word']
			triples.append({'subject':nsubj,'predicate':root,'object':dobj})
			nsubj = None
			root = None
			dobj = None
		else:
			break
	# subj-root+prep-obj (NOT NECESSARILY ROOT!!)
	nsubj = None
	root = None
	dobj = None
	marked = []
	while True:
		for triple in structuredSentence:
			if triple['predicate'] == 'root':
				root = triple
				#marked.append(triple)
				break
		for triple in structuredSentence:
			if triple['predicate'].startswith('nsubj') and root != None and triple['subject'] == root['object']:
				nsubj = triple
				#marked.append(triple)
				break
		for triple in structuredSentence:
			if triple not in marked and triple['predicate'].startswith('prep_') and root != None and triple['subject'] == root['object']:
				prep = triple['predicate'][5:]
				dobj = triple
				marked.append(triple)
				break
		if nsubj != None and nsubj['object'] in wordMap and root != None and root['object'] in wordMap and dobj != None and dobj['object'] in wordMap:#Here, dobj!=None is same as prep != None
			nsubj = wordMap[nsubj['object']]['word']
			root = wordMap[root['object']]['word']+'_'+prep
			dobj = wordMap[dobj['object']]['word']
			triples.append({'subject':nsubj,'predicate':root,'object':dobj})
			nsubj = None
			root = None
			dobj = None
		else:
			break
	return triples

def joinWord(word):
	word = word.replace(' ','_')
	return word.strip('_')

origFile = sys.argv[1]
depParseFile = sys.argv[2]
triplesFile = sys.argv[3]

originalSentences = []
sentences = []

readInput(origFile,depParseFile)
f = open(triplesFile,'w')
for i in range(len(originalSentences)):
	origSent = originalSentences[i]
	sent = sentences[i]
	posTaggedSent = getPOSTags(origSent)
	structuredSentence,wordMap = preprocess(posTaggedSent,sent)
	#getDependencies(structuredSentence,wordMap,0)
	allTriples = getTriples(structuredSentence,wordMap)
	"""print structuredSentence
	print wordMap
	print allTriples
	print """""
	for triple in allTriples:
		f.write('ex:'+joinWord(triple['subject'])+'\t\tex:'+joinWord(triple['predicate'])+'\t\tex:'+joinWord(triple['object'])+'\n')
	f.write('\n')
f.close()
