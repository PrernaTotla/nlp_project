package coref;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import opennlp.tools.cmdline.parser.ParserTool;
import opennlp.tools.coref.DefaultLinker;
import opennlp.tools.coref.DiscourseEntity;
import opennlp.tools.coref.Linker;
import opennlp.tools.coref.LinkerMode;
import opennlp.tools.coref.mention.DefaultParse;
import opennlp.tools.coref.mention.Mention;
import opennlp.tools.parser.AbstractBottomUpParser;
import opennlp.tools.parser.Parse;
import opennlp.tools.parser.Parser;
import opennlp.tools.parser.ParserFactory;
import opennlp.tools.parser.ParserModel;
import opennlp.tools.sentdetect.SentenceDetector;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.InvalidFormatException;
import opennlp.tools.util.Span;

public class corefernce {

	public static void main(String[] args) {

		corefernce detect = new corefernce();
		String[] sentences = {"Harry Potter was a wizard ."," Harry Potter studied in Hogwarts ."," He killed Lord Voldemort ."};
		
	DiscourseEntity[] dt = detect.findEntityMentions(sentences);
		for(int i=0;i<dt.length;i++)
			System.out.println(dt[i]);

	}

public void detect(String sentence){
	SentenceDetector _sentenceDetector = null;
	 
	InputStream modelIn = null;
	try {
	   // Loading sentence detection model
		modelIn = new FileInputStream("./resources/en-sent.bin");
	   final SentenceModel sentenceModel = new SentenceModel(modelIn);
	   modelIn.close();
	   _sentenceDetector = new SentenceDetectorME(sentenceModel);
	   String sentences[] = _sentenceDetector.sentDetect(sentence);
	} catch (final IOException ioe) {
	   ioe.printStackTrace();
	} finally {
	   if (modelIn != null) {
	      try {
	         modelIn.close();
	      } catch (final IOException e) {} // oh well!
	   }
	}
	_sentenceDetector.sentDetect(sentence);
}

public Tokenizer Tokenize(String sentence){
	Tokenizer _tokenizer = null; 
	InputStream modelIn = null;
	try {
	   // Loading tokenizer model
	   modelIn = new FileInputStream("./resources/en-token.bin");
	   final TokenizerModel tokenModel = new TokenizerModel(modelIn);
	   modelIn.close();	 
	   _tokenizer = new TokenizerME(tokenModel);

	} catch (final IOException ioe) {
	   ioe.printStackTrace();
	} finally {
	   if (modelIn != null) {
	      try {
	         modelIn.close();
	      } catch (final IOException e) {} // oh well!
	   }
	}

	return _tokenizer;
}

	private Parser _parser = null;

	private Parse parse(final Parse p) {
	   // lazy initializer
	   if (_parser == null) {
	      InputStream modelIn = null;
	      try {
	         // Loading the parser model
	         modelIn = new FileInputStream("./resources/en-parser-chunking.bin");
	         final ParserModel parseModel = new ParserModel(modelIn);
	         modelIn.close();      
	         _parser = ParserFactory.create(parseModel);
	      } catch (final IOException ioe) {
	         ioe.printStackTrace();
	      } finally {
	         if (modelIn != null) {
	            try {
	               modelIn.close();
	            } catch (final IOException e) {} // oh well!
	         }
	      }
	   }
	   return _parser.parse(p);
	}


private Parse parseSentence(final String text) {
	corefernce token = new corefernce();
	Tokenizer _tokenizer = token.Tokenize(text);
	   final Parse p = new Parse(text,
	         // a new span covering the entire text
         new Span(0, text.length()),
         // the label for the top if an incomplete node
         AbstractBottomUpParser.INC_NODE,
         // the probability of the parse..
         1,
         // the token index of the head of this parse
         0);
 
   // using the tokenize model to use the tokenizePos() method
   final Span[] spans = _tokenizer.tokenizePos(text);
 
   for (int idx=0; idx < spans.length; idx++) {
      final Span span = spans[idx];
      // flesh out the parse with individual token sub-parses 
      p.insert(new Parse(text,
            span,
            AbstractBottomUpParser.TOK_NODE, 
            0,
            idx));
   }
   Parse actualParse = parse(p);
  Parse topParses[] = ParserTool.parseLine(text, _parser, 1);
   
	for (Parse parse : topParses)
		parse.show();
return actualParse;
}

public DiscourseEntity[] findEntityMentions(final String[] sentences) {
	   Linker _linker = null;	 
		try {
			//Loading the linker object
		   _linker = new DefaultLinker("./resources/Models", LinkerMode.TEST);
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   // list of document mentions
	   final List<Mention> document = new ArrayList<Mention>();
	 
	   for (int i=0; i < sentences.length; i++) {
	      // generate the sentence parse tree
		   final Parse parse = parseSentence(sentences[i]);
	       
	      final DefaultParse parseWrapper = new DefaultParse(parse, i);
	      final Mention[] extents = _linker.getMentionFinder().getMentions(parseWrapper);
	 
	      //taken from TreebankParser source...
	      for (int ei=0, en=extents.length; ei<en; ei++) {
	         // construct parses for mentions which don't have constituents
	         if (extents[ei].getParse() == null) {
	            // getting thehead index
	            final Parse snp = new Parse(parse.getText(), 
	                  extents[ei].getSpan(), "NML", 1.0, 0);
	            parse.insert(snp);
	            // setting a new Parse for the current extent
	            extents[ei].setParse(new DefaultParse(snp, i));
	         }
	      }
	      document.addAll(Arrays.asList(extents));
	   }
	 
	   if (!document.isEmpty()) {
		   Mention doc[] = new Mention[document.size()];
		   DiscourseEntity[] dt = _linker.getEntities(document.toArray(doc));
	      return dt;
	   }
	   return new DiscourseEntity[0];
	}
}
