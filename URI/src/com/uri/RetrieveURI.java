package com.uri;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.WordNetDatabase;

/**
 * To get the URI for all the terms in the triples except Proper nouns
 */
public class RetrieveURI
{

	/**
	 * This method checks if the term has a URI in DBPedia namespace by hitting DBPedia resource URI for that term.
	 * If a valid response is received, the URI is for the term
	 * If not, we retrieve the synonyms and related words of the term from wordnet and check if they have a URI
	 * If no URI is found, we use dummy URI for the term
	 */
	public static void main(String[] args)
	{
		String parts[] = {"Killed","Studied"};
		URL url;
		//lookup(parts);
		for(int i=0;i<parts.length;i++) {
			System.out.println(parts[i]);
			String urlString = "";
			try {
				urlString = "http://dbpedia.org/page/"+parts[i].replace(" ", "_");
				url = new URL(urlString);
				URLConnection yc;
				yc = url.openConnection();
				BufferedReader in;
				in = new BufferedReader(new InputStreamReader(
				                            yc.getInputStream()));
				if (in.readLine() != null) 
		            System.out.println(urlString);
					System.out.println();
		        in.close();
			} catch (FileNotFoundException e) {
				String[] synonyms = getSynonyms(parts[i]);
				int j=0;
				if(synonyms != null) {
					for(j=0;j<synonyms.length;j++) {
						try {
								System.out.println(synonyms[j]);
								url = new URL(urlString);
								URLConnection yc;
								yc = url.openConnection();
								BufferedReader in;
								in = new BufferedReader(new InputStreamReader(
								                            yc.getInputStream()));
								if (in.readLine() != null) 
						            System.out.println(urlString);
									System.out.println();
						        in.close();
						        break;
							} catch (FileNotFoundException e1) {
								continue;
							} catch (IOException e1) {
								e1.printStackTrace();
							}
					}
					
					if (j==synonyms.length) {
						System.out.println("URI for this word not found");
						System.out.println();
					}
				} else {
					System.out.println("Synonyms not found");
					System.out.println("URI for this word not found");
					System.out.println();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
	    }
	    
	}
/*	private static void lookup(String[] parts) {
	}*/
	/**
	 * This method gives synonyms for the terms
	 * @return String[]
	 */
	public static String[] getSynonyms(String word) {
		String[] wordForms= null;
		if (word != null && word.length() != 0)
		{
			WordNetDatabase database = WordNetDatabase.getFileInstance();
			Synset[] synsets = (Synset[]) database.getSynsets(word);
			if (synsets.length > 0)
			{
				wordForms = synsets[0].getWordForms();
			}
		}
		return wordForms;
	}

}