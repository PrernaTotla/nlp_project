Team 67 - Prerna Dwivedi, Hetal Mandavia, Prerna Totla

Contributions:

Prerna Dwivedi:

- Wikidump data collection(data/ folder)

- Used Wikipedia to Text tool to remove wiki markup, annotations and sections      containing only links and references. 

- Mapping to DBpedia Triples using the DBPedia lookup tool for proper nouns (nlp.py)

- "uri" project to map the remaining POS entities to DBPedia URIs. 


Hetal Mandavia:

- Coreference Resolution (coref/ folder)

- Used sentence detector, tokenizer, parser, named entity recognition and coreference entity resolution and rewriting into a file as a part of this step.

Prerna Totla:

- Dependency Parsing (dependency_parsing/ folder)

- Hand-written rules to modify dependency tree (parseToTriples.py script)

- The modified tree has root as the predicate and children as the subject and object, thus forming raw triples.